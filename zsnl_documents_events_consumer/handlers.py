import json

from minty import Base


class BaseHandler(Base):
    def __init__(self, cqrs):
        self.cqrs = None
        self.routing_keys = []

    def get_command_instance(self, event):
        return self.cqrs.get_command_instance(
            event.correlation_id,
            "zsnl_domains.document",
            event.context,
            event.user_uuid,
        )


class DocumentEnqueuedHandler(BaseHandler):
    def __init__(self, cqrs):
        self.routing_keys = [
            "zsnl.v2.zsnl_domains_document.Document.DocumentEnqueued"
        ]
        self.cqrs = cqrs

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.create_documents(
            case_uuid=event.entity_id,
            queue_ids=list(
                json.loads(changes["enqueued_documents_data"]).keys()
            ),
        )
