from minty.cqrs import CQRS
from minty.infrastructure import InfrastructureFactory
from minty.middleware import AmqpPublisherMiddleware
from minty_amqp.client import AMQPClient
from minty_infra_sqlalchemy import DatabaseTransactionMiddleware
from zsnl_domains import document

from .consumers import DocumentsEventsConsumer


def main():
    infra_factory = InfrastructureFactory(config_file="config.conf")
    cqrs = CQRS(
        domains=[document],
        infrastructure_factory=infra_factory,
        command_wrapper_middleware=[
            DatabaseTransactionMiddleware("database"),
            AmqpPublisherMiddleware(
                publisher_name="document", infrastructure_name="amqp"
            ),
        ],
    )

    config = cqrs.infrastructure_factory.get_config(context=None)
    amqp_client = AMQPClient(config, cqrs=cqrs)
    amqp_client.register_consumers([DocumentsEventsConsumer])
    amqp_client.start()


def init():
    if __name__ == "__main__":
        main()


init()
