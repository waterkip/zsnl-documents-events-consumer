from minty_amqp.consumer import BaseConsumer

from .handlers import DocumentEnqueuedHandler


class DocumentsEventsConsumer(BaseConsumer):
    def _register_routing(self):
        self._known_handlers = [DocumentEnqueuedHandler(self.cqrs)]

        self.routing_keys = []
        for handler in self._known_handlers:
            self.routing_keys.extend(handler.routing_keys)
