## Code style tools
black
flake8
isort==4.3.10

## QA (tests, documentation)
sphinx
sphinx_rtd_theme
sphinx-autodoc-typehints

## Release
bumpversion

## Framework
minty==1.2.4
minty-amqp==2.0.0

python-json-logger

##  Project specific requirements

amqpstorm
psycopg2-binary

git+https://gitlab.com/minty-python/zsnl-database.git@v0.1.24

git+https://gitlab.com/minty-python/zsnl_domains.git@v0.5.15

