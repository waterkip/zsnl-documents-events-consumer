from unittest import mock

from zsnl_documents_events_consumer import __main__
from zsnl_documents_events_consumer.consumers import DocumentsEventsConsumer
from zsnl_documents_events_consumer.handlers import (
    BaseHandler,
    DocumentEnqueuedHandler,
)


class TestCaseEventsConsumers:
    def test_base_handler(self):
        base_handler = BaseHandler(mock.MagicMock())
        assert base_handler.routing_keys == []

    def test_documents_enqueued_handler(self):
        mock_cqrs = mock.MagicMock()
        mock_command_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = mock_command_instance
        documents_enqueued_handler = DocumentEnqueuedHandler(mock_cqrs)
        assert documents_enqueued_handler.routing_keys == [
            "zsnl.v2.zsnl_domains_document.Document.DocumentEnqueued"
        ]
        assert documents_enqueued_handler.cqrs == mock_cqrs

        mock_event = mock.MagicMock()
        mock_event.event_name = ("DocumentEnqueued",)
        mock_event.correlation_id = "fake_correlaton_id"
        mock_event.context = "fake_context"
        mock_event.user_uuid = "fake_user_uuid"
        mock_event.entity_id = "fake_uuid"
        mock_event.entity_id
        mock_event.changes = [
            {
                "key": "enqueued_documents_data",
                "old_value": None,
                "new_value": "{}",
            }
        ]

        documents_enqueued_handler.handle(mock_event)
        mock_command_instance.create_documents.assert_called_once_with(
            case_uuid="fake_uuid", queue_ids=[]
        )

    def test_consumer(self):
        documents_events_consumer = DocumentsEventsConsumer(
            queue="fake_queue_name",
            exchange="fake_exchange",
            cqrs=mock.MagicMock(),
            qos_prefetch_count=2,
            dead_letter_config={},
        )
        assert documents_events_consumer.routing_keys == [
            "zsnl.v2.zsnl_domains_document.Document.DocumentEnqueued"
        ]

    @mock.patch("zsnl_documents_events_consumer.__main__.CQRS")
    @mock.patch(
        "zsnl_documents_events_consumer.__main__.InfrastructureFactory"
    )
    @mock.patch("zsnl_documents_events_consumer.__main__.AMQPClient")
    def test_main(self, mock_client, mock_infra, mock_cqrs):
        with mock.patch.object(__main__, "__name__", "__main__"):
            __main__.init()
            mock_client.assert_called()
            mock_client().register_consumers.assert_called_with(
                [DocumentsEventsConsumer]
            )
            mock_client().start.assert_called()
